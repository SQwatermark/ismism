# 【从零开发】主义主义网站

## 代码结构

* `ont` 基础代码 `ontic`
* `eid` 核心代码 `eidetic`
* `pra` 业务代码 `praxic`
* `tst` 测试代码 `tests`

## [系列视频](https://space.bilibili.com/483417795/video)

